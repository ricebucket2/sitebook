# sitebook

Sitebook for ricebucket.life

## Topology

## Initialize the machine
1. Install jumpcloud [if possible](https://support.jumpcloud.com/support/s/article/jumpcloud-agent-compatibility-system-requirements-and-impacts1)
```
-> % ansible-playbook -i inventory.yml extra-playbooks/jumpcloud.yml --limit <host|group> -e "jumpcloud_api=abc123" -u <initial-user> -k -K --check
-> % ansible-playbook -i inventory.yml extra-playbooks/jumpcloud.yml --limit <host|group> -e "jumpcloud_api=abc123" -u <initial-user> -k -K
```

## Devices
### edgerouter
root@edgerouterx:~# curl -qLs https://github.com/WireGuard/wireguard-vyatta-ubnt/releases/download/1.0.20210124-1/e100-v2-v1.0.20210124-v1.0.20200827.deb -o wireguard.deb
root@edgerouterx:~# dpkg -i wireguard.deb
Selecting previously unselected package wireguard.
(Reading database ... 37058 files and directories currently installed.)
Preparing to unpack wireguard.deb ...
Adding 'diversion of /opt/vyatta/share/perl5/Vyatta/Interface.pm to /opt/vyatta/share/perl5/Vyatta/Interface.pm.vyatta by wireguard'
Adding 'diversion of /opt/vyatta/share/vyatta-cfg/templates/firewall/options/mss-clamp/interface-type/node.def to /opt/vyatta/share/vyatta-cfg/templates/firewall/options/mss-clamp/interface-type/node.def.vyatta by wireguard'
Adding 'diversion of /opt/vyatta/share/vyatta-cfg/templates/firewall/options/mss-clamp6/interface-type/node.def to /opt/vyatta/share/vyatta-cfg/templates/firewall/options/mss-clamp6/interface-type/node.def.vyatta by wireguard'
Unpacking wireguard (1.0.20210124-1) ...
Setting up wireguard (1.0.20210124-1) ...

root@edgerouterx:~# wg genkey | tee /dev/tty | wg pubkey
yBf5hKZwEzVvA4iWaBNI6Cdd1c/Pvp4kx0v5mvY50Hw=
3FfNMDd8pYf/4vqhmwLCfo/EMe5oaBDddYYS9EFYUVA=

set interfaces wireguard wg0 route-allowed-ips false; commit
set interfaces wireguard wg0 route-allowed-ips true; commit

### ts140.devices.ricebucket.life
#### Specifications

$ sudo vi /etc/sysctl.d/bridge.conf
net.bridge.bridge-nf-call-ip6tables=0
net.bridge.bridge-nf-call-iptables=0
net.bridge.bridge-nf-call-arptables=0

$ sudo vi /etc/udev/rules.d/99-bridge.rules
ACTION=="add", SUBSYSTEM=="module", KERNEL=="br_netfilter", RUN+="/sbin/sysctl -p /etc/sysctl.d/bridge.conf"


#### GPU Passthrough
https://mathiashueber.com/pci-passthrough-ubuntu-2004-virtual-machine/

```
~$ sudo diff /etc/default/grub.bak /etc/default/grub
10c10
< GRUB_CMDLINE_LINUX_DEFAULT=""
---
> GRUB_CMDLINE_LINUX_DEFAULT="intel_iommu=on"

~$ sudo update-grub
Sourcing file `/etc/default/grub'
Sourcing file `/etc/default/grub.d/init-select.cfg'
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.4.0-64-generic
Found initrd image: /boot/initrd.img-5.4.0-64-generic
Adding boot menu entry for UEFI Firmware Settings
done

~$ dmesg | grep -i IOMMU
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-5.4.0-64-generic root=/dev/mapper/vg--sys-lv--root ro intel_iommu=on
[    0.080249] Kernel command line: BOOT_IMAGE=/boot/vmlinuz-5.4.0-64-generic root=/dev/mapper/vg--sys-lv--root ro intel_iommu=on
[    0.080288] DMAR: IOMMU enabled

~$ lspci -nn | grep -i nvidia
01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GM107 [GeForce GTX 750 Ti] [10de:1380] (rev a2)
01:00.1 Audio device [0403]: NVIDIA Corporation GM107 High Definition Audio Controller [GeForce 940MX] [10de:0fbc] (rev a1)

~$ sudo diff /etc/default/grub.bak /etc/default/grub
10c10
< GRUB_CMDLINE_LINUX_DEFAULT=""
---
> GRUB_CMDLINE_LINUX_DEFAULT="intel_iommu=on kvm.ignore_msrs=1 vfio-pci.ids=10de:1380,10de:0fbc"

~$ sudo update-grub

01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GM107 [GeForce GTX 750 Ti] [10de:1380] (rev a2) (prog-if 00 [VGA controller])
	Subsystem: eVga.com. Corp. GM107 [GeForce GTX 750 Ti] [3842:3753]
	Flags: fast devsel, IRQ 11
	Memory at f6000000 (32-bit, non-prefetchable) [disabled] [size=16M]
	Memory at e0000000 (64-bit, prefetchable) [disabled] [size=256M]
	Memory at f0000000 (64-bit, prefetchable) [disabled] [size=32M]
	I/O ports at e000 [disabled] [size=128]
	Expansion ROM at f7000000 [disabled] [size=512K]
	Capabilities: <access denied>
	Kernel driver in use: vfio-pci
	Kernel modules: nvidiafb, nouveau

01:00.1 Audio device [0403]: NVIDIA Corporation GM107 High Definition Audio Controller [GeForce 940MX] [10de:0fbc] (rev a1)
	Subsystem: eVga.com. Corp. GM107 High Definition Audio Controller [GeForce 940MX] [3842:3753]
	Flags: bus master, fast devsel, latency 0, IRQ 10
	Memory at f7080000 (32-bit, non-prefetchable) [size=16K]
	Capabilities: <access denied>
	Kernel driver in use: vfio-pci
	Kernel modules: snd_hda_intel

```


## Windows Client setups
http://azertech.net/content/kvm-qemu-qcow2-qemu-img-and-snapshots
https://blog.programster.org/qemu-img-cheatsheet
https://dustymabe.com/2015/01/11/qemu-img-backing-files-a-poor-mans-snapshotrollback/

### enable scripts on powershell
Set-ExecutionPolicy RemoteSigned -Scope LocalMachine

### rename
Rename-Computer -NewName "WIN10-TS140"

### restarting computer
Restart-Computer

### show all icons
```
shell:::{05d7b0f4-2121-4eff-bf6b-ed3f69b894d9}
```

### Virtio drivers
https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso


PS C:\Windows\system32> Set-TimeZone -Name "Eastern Standard Time"
PS C:\Windows\system32> Get-TimeZone


Id                         : Eastern Standard Time
DisplayName                : (UTC-05:00) Eastern Time (US & Canada)
StandardName               : Eastern Standard Time
DaylightName               : Eastern Daylight Time
BaseUtcOffset              : -05:00:00
SupportsDaylightSavingTime : True


qemu-img create -f qcow2 Base.img 50G
qemu-img create -f qcow2 -F qcow2 -b Base.img FirstFloor.ovl
qemu-img create -f qcow2 -F qcow2 -b FirstFloor.ovl SecondFloor.ovl
qemu-img create -f qcow2 -F qcow2 -b SecondFloor.ovl Roof.ovl




/ # certbot certonly --webroot -w /var/www/letsencrypt -d bitwarden.ricebucket.life -m letsencrypt@ricebucket.life --agree-tos


# nginx
https://gist.github.com/plentz/6737338

https://eshop.macsales.com/blog/46348-how-to-create-and-use-a-ram-disk-with-your-mac-warnings-included/
diskutil erasevolume HFS+ "RAMDisk" `hdiutil attach -nomount ram://2097152`
256 MB = 524288
512 MB = 1048576
1 GB = 2097152
2 GB = 4194304


gpg> setpref SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
